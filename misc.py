from openerp.tools.translate import _
import subprocess
import re
import logging
_logger = logging.getLogger(__name__)


YEAR = [
    ('1', '1'),
    ('2', '2'),
]

PERIOD = [
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
]

DURATION = [
    ('12', '12'),
    ('6', '6'),
    ('4', '4'),
    ('3', '3'),
]

GENDER = [
    ('male', _('Male')),
    ('female', _('Female')),
]

MEASURE_UNIT = [
    ('time', _('Time')),
    ('number', _('Number')),
]

TIME_CONTROL = [
    ('between', _('Between')),
    ('exceed', _('Exceed')),
    ('not exceed', _('Not Exceed')),
]

SCORE_STATE = [
    ('draft', _('Draft')),
    ('published', _('Published')),
    ('for review', _('For review')),
    ('approved', _('Approved')),
]

CONTROL_STATE = [
    ('draft', _('Draft')),
    ('published', _('Published')),
    ('settled', _('Settled')),
]

KIND_OF_MERIT = [
    ('medal', _('Medal')),
    ('recognition', _('Recognition')),
]

COURSE_STATE = [
    ('planned', 'Planned'),
    ('running', 'Running'),
    ('finalized', 'Finalized'),
]


ENROLLMENT_STATE = [
    ('enrolled', 'Enrolled'),
    ('retired', 'Retired'),
    ('failed', 'Failed'),
]

FACULTY_TYPE = [
    ('civil', 'Civil'),
    ('military', 'Military'),
]

BLOOD_TYPE = [
    ('O-', 'O Rh-'),
    ('O+', 'O Rh+'),
    ('A-', 'A Rh-'),
    ('A+', 'A Rh+'),
    ('B-', 'B Rh-'),
    ('B+', 'B Rh+'),
    ('AB-', 'AB Rh-'),
    ('AB+', 'AB Rh+'),
]

time_value_pattern = re.compile(r'^[0-9]+,[0-5][0-9]$')
number_value_pattern = re.compile(r'^[0-9]+$')


def _get_ir_model_data_id(self, cr, uid, model, name):
    model_obj = self.pool.get('ir.model.data')
    result = model_obj._get_id(cr, uid, model, name)
    return model_obj.browse(cr, uid, result).res_id


def _get_ip(self, cr, uid, context=None):
    # local = socket.gethostbyname(socket.gethostname())
    process = subprocess.Popen(["ifconfig"], stdout=subprocess.PIPE)
    ifc = process.communicate()
    pattern = re.compile(r'inet\s*\w*\S*:\s*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')
    res = pattern.findall(ifc[0])
    ip = ','.join(res)
    return ip

def _check_verification_identification_id(identification):
    l = len(identification)
    if l == 10 or l == 13:  # verificar la longitud correcta
        cp = int(identification[0:2])
        if cp >= 1 and cp <= 22:  # verificar codigo de provincia
            tercer_dig = int(identification[2])
            if tercer_dig >= 0 and tercer_dig < 6:  # numeros enter 0 y 6
                if l == 10:
                    return _verification_identification_id(identification, 0)
                elif l == 13:
                    return _verification_identification_id(identification, 0) and identification[10:13] != '000' # se verifica q los ultimos numeros no sean 000
            elif tercer_dig == 6:
                return _verification_identification_id(identification, 1)  # sociedades publicas
            elif tercer_dig == 9:  # si es ruc
                return _verification_identification_id(identification, 2)  # sociedades privadas
            else:
                return False
        else:
            return False
    else:
        return False

def _verification_identification_id(identification, type):
    total = 0
    unicodestring = identification
    identification_id = str(unicodestring).encode("utf-8")
    if type == 1: # r.u.c. publicos
        base = 11
        d_ver = int(identification_id[8])
        _logger.warning("digito ver %d" % d_ver)
        multip = (3, 2, 7, 6, 5, 4, 3, 2 )
        longitude = 8
    elif type == 2: # r.u.c. juridicos y extranjeros sin cedula
        base = 11
        d_ver = int(identification_id[9])
        _logger.warning("digito ver %d" % d_ver)
        multip = (4, 3, 2, 7, 6, 5, 4, 3, 2)
        longitude = 9
    else: # cedula y r.u.c persona natural
        base = 10
        d_ver = int(identification_id[9])# digito verificador
        multip = (2, 1, 2, 1, 2, 1, 2, 1, 2)
        longitude = 9
    for i in range(0, longitude):
        a = int(identification_id[i])
        _logger.warning(a)
        b = multip[i]
        p = a * b
        if type == 0:
            total += p if p < 10 else int(str(p)[0])+int(str(p)[1])
        else:
            total += p
    mod = total % base
    val = base - mod if mod != 0 else 0
    return val == d_ver