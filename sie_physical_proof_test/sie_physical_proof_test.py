from openerp import fields, models
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp


class sie_physical_proof_test(models.Model):
    _name = 'sie.physical.proof.test'
    _description = 'Test'

    name = fields.Char('Test', size=96, required=True)
    score = fields.Float('Score', required=True, digits_compute=dp.get_precision('Score'))

    _defaults = {
    'score': lambda *args: 100,
    }

    _sql_constraints = [
        ('name_uk', 'unique(name)', 'Test must be unique'),
        ('score_ck', 'check(score > 0)', 'Score must be greater than 0'),
    ]

    def copy(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default = default.copy()
        current = self.browse(cr, uid, id, context=context)
        default.update({'name': current.name + _(' (Copy)')})
        return super(sie_physical_proof_test, self).copy(cr, uid, id, default, context=context)