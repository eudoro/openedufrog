#! -*- encoding: utf-8 -*-
##############################################################################
#
# Manexware S.A., Open Source Company
# Copyright (C) 2014 Manuel Vega Ulloa All Rights Reserved
# http://ec.linkedin.com/pub/manuel-vega/8/289/944/
# $Id$
#
#
##############################################################################
import uuid
import string
from random import *
import time
import re
import logging

from openerp import addons
from openerp.osv import fields, osv
from openerp import tools
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.tools.sql import drop_view_if_exists
from ..misc import _get_ir_model_data_id


_logger = logging.getLogger(__name__)

########################################################################
class sie_score_per_subject_wizard(osv.osv_memory):
    _name = 'sie.score.report.per.subject.wizard'
    _description = 'Wizard for Score Report per Subject'

    def _get_subjects(self, cr, uid, ids, prop, unknow_none, context=None):
        res = {}
        for rec in self.browse(cr, uid, ids):
            res[rec.id] = _get_subjects_per_period(cr, uid, ids, rec.period_id.id, context)
        return res

    _columns = {
    'course_id': fields.many2one('sie.course', 'Course', required=True),
    'enrollment_id': fields.many2one('sie.enrollment', 'Classroom', domain="[('course_id', '=', course_id)]",
                                     required=True),
    # 'subjects': fields.many2many('sie.subject', 'sie_score_report_per_subject_wizard_rel', 'wizard_id', 'subject_id', 'Subjects', required=True),
    #'subjects': fields.function(_get_subjects, type='many2many', relation='sie.subject', string='Subjects'),
    'student_id': fields.many2one('hr.employee', 'Student', required=True),
    'details': fields.one2many('sie.score.report.per.subject.details', 'wizard', 'Details'),
    }

    def onchange_period(self, cr, uid, ids, period=False, context=None):
        value = {'subjects': False}
        value['subjects'] = _get_subjects_per_period(cr, uid, ids, period, context)
        return {'value': value}

    def action_find(self, cr, uid, ids, context=None):
        current = self.browse(cr, uid, ids[0])
        # _logger.info(current)
        cr.execute("""SELECT period_id, subject_id, score, coefficient, (score * coefficient) AS total
			FROM sie_subject_score_student_view 
			WHERE course_id = %s AND enrollment_id = %s AND student_id = %s
			ORDER BY period_id, subject_id""" %
                   (current.course_id.id, current.enrollment_id.id, current.student_id.id))
        details = []
        for period, subject, score, coefficient, total in cr.fetchall():
            details.append({
            'period': period,
            'subject': subject,
            'score': score,
            'coefficient': coefficient,
            'total': total,
            })
        context.update({
        'default_course_id': current.course_id.id,
        'default_enrollment_id': current.enrollment_id.id,
        'default_student_id': current.student_id.id,
        'default_details': details
        })
        #
        view_id = _get_ir_model_data_id(self, cr, uid, 'sie_ratings', 'view_sie_score_report_per_subject_details_form')
        #_logger.warning(search_view_id)
        return {
        'type': 'ir.actions.act_window',
        'name': _('Score Report per Subject'),
        'view_type': 'form',
        'view_mode': 'form',
        'res_model': 'sie.score.report.per.subject.wizard',
        'views': [(view_id, 'form')],
        'target': 'new',
        'context': context,
        }

    def action_print(self, cr, uid, ids, context=None):
        current = self.browse(cr, uid, ids[0])
        total = 0
        for d in current.details:
            total += d.total
        context.update({'total': total})
        # _logger.info(total)
        return {
        'type': 'ir.actions.report.xml',
        'report_name': 'score_per_subject_report',
        'context': context,
        }


sie_score_per_subject_wizard()

########################################################################
class sie_score_per_subject_details(osv.osv_memory):
    _name = 'sie.score.report.per.subject.details'
    _description = 'Score Report per Subject'

    _columns = {
    'period': fields.many2one('sie.course.period', 'Period', readonly=True),
    'subject': fields.many2one('sie.subject', 'Subject', readonly=True),
    'score': fields.float('Score', digits_compute=dp.get_precision('Total'), readonly=True),
    'coefficient': fields.float('Coefficient', digits_compute=dp.get_precision('Calculation'), readonly=True),
    'total': fields.float('Total', digits_compute=dp.get_precision('Total'), readonly=True),
    'wizard': fields.many2one('sie.score.report.per.subject.wizard', 'Wizard'),
    }


sie_score_per_subject_details()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: