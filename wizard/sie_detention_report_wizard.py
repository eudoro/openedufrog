#! -*- encoding: utf-8 -*-
##############################################################################
#
# Manexware S.A., Open Source Company
# Copyright (C) 2014 Manuel Vega Ulloa All Rights Reserved
# http://ec.linkedin.com/pub/manuel-vega/8/289/944/
# $Id$
#
#
##############################################################################
import time

from openerp.osv import fields, osv


########################################################################
class sie_detention_report_wizard(osv.osv_memory):
    _name = 'sie.detention.report.wizard'
    _description = 'Wizard for Report Arrests'

    _columns = {
    'sanction_id': fields.many2one('sie.fault.sanction', 'Sanction', required=True),
    'classification_id': fields.related('sanction_id', 'classification_id', relation='sie.fault.classification',
                                        string='Classification of Fault', type='many2one'),
    'from': fields.date('From', required=True),
    'to': fields.date('To', required=True),
    }

    _defaults = {
    'from': lambda *args: (date.today() + timedelta(-7)).strftime('%Y-%m-%d'),
    'to': lambda *args: time.strftime('%Y-%m-%d'),
    }


sie_detention_report_wizard()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: