from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError


class sie_subject_summary(models.Model):
    _name = 'sie.subject.summary'
    _description = 'Summary'

    name = fields.Char('Title', size=128, required=True)
    description = fields.Text('Description')
    parent_id = fields.Many2one('sie.subject.summary', 'Parent', ondelete='cascade')
    child_ids = fields.One2many('sie.subject.summary', 'parent_id', 'Summary')
    subject_id = fields.Many2one('sie.subject', 'Subject', ondelete='cascade')

    @api.one
    @api.constrains('parent_id', 'subject_id')
    def _check_summary_recursion(self):
        if self.parent_id:
            if not self.subject_id:
                raise ValidationError(_('Error! You cannot create recursive summary of subject'))