from openerp import fields, models, api
from openerp.exceptions import ValidationError
import time, re
import logging
_logger = logging.getLogger(__name__)


class sie_subject(models.Model):
    _name = 'sie.subject'
    _description = 'Subject'

    name = fields.Char('Subject', size=96, required=True, search='_search_name')
    code = fields.Char('Code', size=10, search='_search_code', required=True)
    description = fields.Text('Description')
    version = fields.Float('Version', digits=(2, 1), required=True)
    last_review = fields.Date('Last review', readonly=True)
    senescyt_code = fields.Char('SENESCYT\'s code', size=64, search='_search_senescyt_code')
    shaft_id = fields.Many2one(comodel_name='sie.training.shaft', string='Shaft of training', ondelete='set null',
                               required=True)
    summary_ids = fields.One2many(comodel_name='sie.subject.summary', inverse_name='subject_id', string='Summary')
    unit_ids = fields.One2many(comodel_name='sie.subject.unit', inverse_name='subject_id',
                               string='Learning Units', required=True)
    hours = fields.Integer(compute='_compute', string='No Hours', store=True)
    credits = fields.Integer(compute='_compute', string='Credits', store=True)
    responsible_id = fields.Many2one(comodel_name='sie.faculty', string='Responsible', ondelete='restrict')
    faculty_ids = fields.Many2many(comodel_name='sie.faculty', string='Faculties')

    _order = 'name, version'

    _defaults = {
        'version': lambda *args: 1.0,
        'last_review': lambda *args: time.strftime('%Y-%m-%d'),
    }

    _sql_constraints = [
        ('subject_uk', 'unique(name, senescyt_code, version)', 'Subject must be unique'),
        ('version_ck', 'check(version >= 1)', 'Version must be equal or greater than 1'),
    ]



    @api.multi
    @api.depends('unit_ids', 'hours', 'credits')
    def _compute(self):
        if self.unit_ids:
            self.hours = sum(record.hours for record in self.unit_ids)
            self.credits = self.hours / 16
            _logger.info(self.credits)

    @api.one
    @api.depends('name', 'version', 'senescyt_code')
    def _compute_display_name(self):
        self.display_name = '%s (v%.1f)' % (self.name, self.version)
        if self.senescyt_code:
            self.display_name = '%s [%s]' % (self.display_name, self.senescyt_code)


    def _search_name(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        if self.name:
                return [('name', operator, value)]

    def _search_code(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        if self.code:
            pattern = re.compile(r'^[0-9]+$')
            match = pattern.match(self.code)
            if match:
                if match.group() == self.code:
                    return [('code', operator, value)]

    def _search_senescyt_code(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        if self.senescyt_code:
                pattern = re.compile(r'^[0-9]+$')
                match = pattern.match(self.senescyt_code)
                if match:
                    if match.group() == self.senescyt_code:
                        return ('senescyt_code', operator, self.senescyt_code)

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.version
        if not copied_count:
            new_version = 1
        else:
            new_version = copied_count + 0.1
        default['version'] = new_version
        return super(sie_subject, self).copy(default)

    @api.model
    def create(self, vals):
        vals['name'] = vals['name'].title()
        return super(sie_subject, self).create(vals)

    @api.multi
    def write(self, vals):
        name = False
        if vals.get('name'):
            name = vals['name']
        else:
            name = self.name
        # _logger.info(name)
        vals.update({'last_review': time.strftime('%Y-%m-%d'), 'name': name.title()})
        return super(sie_subject, self).write(vals)