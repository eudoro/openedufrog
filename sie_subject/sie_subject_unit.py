from openerp import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)


class sie_subject_unit(models.Model):
    _name = 'sie.subject.unit'
    _description = 'Learning Unit'

    name = fields.Char('Title', size=128, required=True)
    number = fields.Integer('Number', required=True)
    description = fields.Text('Description')
    parent_id = fields.Many2one('sie.subject.unit', 'Parent', ondelete='cascade')
    child_ids = fields.One2many('sie.subject.unit', 'parent_id', 'Learning SubUnits')
    subject_id = fields.Many2one('sie.subject', 'Subject', ondelete='cascade')
    hours = fields.Integer('No. Hours')
    total_hours = fields.Integer(compute='_total_hours', string='Total hours', store=True)
    last_child = fields.Boolean(string='Last child?')

    _defaults = {
        'hours': lambda *args: 0,
        'last_child': lambda *args: True,
    }

    _sql_constraints = [
        ('number_uk', 'unique(number, parent_id)', 'Number must be unique per learning unit'),
        ('hours_ck', 'check(hours >= 0)', '# hours must be equal or greater than 0'),
    ]

    @api.one
    @api.depends('total_hours', 'child_ids')
    def _total_hours(self):
        if self.child_ids:
            self.total_hours = sum(o.hours for o in self.child_ids)
        else:
            self.total_hours = 0

    @api.onchange('total_hours')
    def _onchange_total_hours(self):
        if self.total_hours > 0:
            self.hours = self.total_hours
            self.last_child = False
        else:
            self.hours = 0
            self.last_child = True