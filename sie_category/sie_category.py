from openerp import models, fields
from openerp.tools.translate import _


class sie_category(models.Model):
    _name = 'sie.category'

    name = fields.Char(string='Name', required=True, select=True)

    _sql_constraints = [
        ('name_uk', 'unique(name)', _('Name must be unique'))
    ]