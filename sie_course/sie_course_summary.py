from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import ValidationError


class sie_course_summary(models.Model):
    _name = 'sie.course.summary'
    _description = 'Summary'

    name = fields.Char('Title', size=128, required=True)
    description = fields.Text('Description')
    parent_id = fields.Many2one('sie.course.summary', 'Parent', ondelete='cascade')
    child_ids = fields.One2many('sie.course.summary', 'parent_id', 'Summary')
    course_id = fields.Many2one('sie.course', 'Course', ondelete='cascade')

    @api.one
    @api.constrains('parent_id', 'course_id')
    def _check_summary_recursion(self):
        if self.parent_id:
            if not self.course_id:
                raise ValidationError(_('Error! You cannot create recursive summary of course'))
