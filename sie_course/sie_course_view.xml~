<?xml version="1.0" encoding="utf-8"?>
<openerp>
    <data>

    	<!-- Course -->
    	<record id="view_sie_course_form" model="ir.ui.view">
    		<field name="name">sie.course.form</field>
            <field name="model">sie.course</field>
            <field name="arch" type="xml">
            	<form string="Course" version="7.0">
            		<header>
            			<field name="state" widget="statusbar" nolabel="1" />
            		</header>
            		<sheet>
                        <group col="6">
                            <field name="name" />
                            <field name="promo" />
                            <field name="specialty_id" options="{'no_open':True}" />
                            <field name="start_date" />
                            <field name="end_date" />
                            <!-- <newline /> -->
                            <field name="minimum" />
                        </group>
            			<notebook>
                            <page string="Summary">
                                <field name="summary_ids" nolabel="1" />
                            </page>
                            <page string="Periods">
                                <field name="period_ids" nolabel="1" />
                            </page>
                            <page string="Parameters">
                                <field name="parameter_ids" nolabel="1" />
                            </page>
<page string="Conduct">
            			<group col="6">
	            			<field name="min_conduct" />
	            			<field name="max_demerits" />
	            			<field name="coefficient" />
	            		</group>
            		</page>
                        </notebook>
            		</sheet>
            	</form>
            </field>
    	</record>
    	<record id="view_sie_course_tree" model="ir.ui.view">
    		<field name="name">sie.course.tree</field>
            <field name="model">sie.course</field>
            <field name="arch" type="xml">
            	<tree string="Course" colors="blue:state == 'running';gray:state == 'planned'">
                    <field name="name" />
                    <field name="promo" />
                    <field name="specialty_id" />
                    <field name="state" />
                </tree>
            </field>
    	</record>
    	<record id="view_sie_course_search" model="ir.ui.view">
    		<field name="name">sie.course.search</field>
            <field name="model">sie.course</field>
            <field name="arch" type="xml">
            	<search string="Search Course">
                    <filter string="First Year" domain="[('name','=','1')]" />
                    <filter string="Second Year" domain="[('name','=','2')]" />
            		<filter string="Planned" domain="[('state','=','planned')]" />
                    <filter string="Running" domain="[('state','=','running')]" />
                    <filter string="Finalized" domain="[('state','=','finalized')]" />
                    <field name="specialty_id" />
            		<group expand="0" string="Group By...">
            			<filter string="Year" context="{'group_by': 'name'}" />
                        <filter string="Specialty" context="{'group_by': 'specialty_id'}" />
                        <filter string="State" context="{'group_by': 'state'}" />
            		</group>
            	</search>
            </field>
    	</record>
    	<record id="action_sie_course" model="ir.actions.act_window">
    		<field name="name">Course</field>
            <field name="res_model">sie.course</field>
            <field name="view_type">form</field>
            <field name="view_mode">tree,form</field>
            <field name="search_view_id" ref="view_sie_course_search" />
            <field name="help" type="html">
              <p class="oe_view_nocontent_create">
                Click to start a new course.
              </p>
            </field>
    	</record>
    	<menuitem id="menu_action_sie_course" 
    		action="action_sie_course" 
    		parent="menu_sie_school"
    		groups="group_sie_coordinator"
    		sequence="2" />

        <!-- Summary -->
        <record id="view_sie_course_summary_form" model="ir.ui.view">
            <field name="name">sie.course.summary.form</field>
            <field name="model">sie.course.summary</field>
            <field name="arch" type="xml">
                <form string="Summary" version="7.0">
                    <sheet>
                        <group>
                            <field name="name" />
                            <field name="description" />
                        </group>
                        <notebook>
                            <page string="Summary">
                                <field name="child_ids" nolabel="1" />
                            </page>
                        </notebook>
                    </sheet>
                </form>
            </field>
        </record>
        <record id="view_sie_course_summary_tree" model="ir.ui.view">
            <field name="name">sie.course.summary.tree</field>
            <field name="model">sie.course.summary</field>
            <field name="arch" type="xml">
                <tree string="Summary">
                    <field name="name" />
                </tree>
            </field>
        </record>

        <!-- Period -->
        <record id="view_sie_course_period_form" model="ir.ui.view">
            <field name="name">sie.course.period.form</field>
            <field name="model">sie.course.period</field>
            <field name="arch" type="xml">
                <form string="Period" version="7.0">
                    <sheet>
                        <group col="4">
                            <field name="name" />
                            <newline />
                            <field name="start_date" />
                            <field name="end_date" />
                            <field name="duration" />
                            <newline />
                            <field name="subject_ids" widget="many2many_tags" colspan="4" />
                            <field name="total_credits" />
                            <field name="total_hours" />
                        </group>
                    </sheet>
                </form>
            </field>
        </record>
        <record id="view_sie_course_period_tree" model="ir.ui.view">
            <field name="name">sie.course.period.tree</field>
            <field name="model">sie.course.period</field>
            <field name="arch" type="xml">
                <tree string="Period">
                    <field name="name" />
                    <field name="start_date" />
                    <field name="end_date" />
                    <field name="no_of_subject" />
                    <field name="total_credits" />
                </tree>
            </field>
        </record>

        <!-- Parameters -->
        <record id="view_sie_course_parameter_form" model="ir.ui.view">
            <field name="name">sie.course.parameter.form</field>
            <field name="model">sie.course.parameter</field>
            <field name="arch" type="xml">
                <form string="Parameters" version="7.0">
                    <sheet>
                        <div class="oe_title">
                            <label for="name" class="oe_edit_only" string="Name" />
                            <h1>
                                <field name="name" class="oe_inline" placeholder="Parameter" />
                            </h1>
                        </div>
                        <group col="4">
                            <field name="description" colspan="4" />
                            <field name="coefficient" />
                            <!--attrs="{'readonly':[('last_child','=',False)]}" 
                            <field name="last_child" invisible="1" /-->
                        </group>
                        <notebook>
                            <page string="Child Parameters">
                                <field name="child_ids" nolabel="1" />
                            </page>
                        </notebook>
                    </sheet>
                </form>
            </field>
        </record>
        <record id="view_sie_course_parameter_tree" model="ir.ui.view">
            <field name="name">sie.course.parameter.tree</field>
            <field name="model">sie.course.parameter</field>
            <field name="arch" type="xml">
                <tree string="Parameters">
                    <field name="name" />
                    <field name="coefficient" />
                </tree>
            </field>
        </record>

    </data>
</openerp>
