from openerp import fields, models, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import ValidationError


class sie_course_parameter(models.Model):
    _name = 'sie.course.parameter'
    _description = 'Course Parameter'

    name = fields.Char(string='Name', compute='_compute_name', store=True)
    level = fields.Integer(compute='_compute_level', string='Level', store=True)
    param_name = fields.Many2one(comodel_name='sie.course.parameter.name', string='Parameter Name', required=True,
                                 domain="[('level', '=', level)]")
    coefficient = fields.Float(string='Coefficient', digits_compute=dp.get_precision('Coefficient'), required=True)
    description = fields.Text(string='Description')
    parent_id = fields.Many2one(comodel_name='sie.course.parameter', string='Parent', ondelete='cascade')
    child_ids = fields.One2many(comodel_name='sie.course.parameter', inverse_name='parent_id', string='Parameter')
    course_id = fields.Many2one(comodel_name='sie.course', string='Course',
                                ondelete='cascade')
    course_ref = fields.Char(string='Course Ref', compute='_compute_course_ref', store=True)
    parent_ref = fields.Char(string='Parent Name', compute='_compute_parent_ref', store=True)

    last_child = fields.Boolean(string='Last child?')
    total_coefficient = fields.Float(compute='_total_coefficient', string='Total Coefficient',
                                     digits_compute=dp.get_precision('Coefficient'), store=True)

    _defaults = {
        'coefficient': lambda *args: 0.1,
        'total_coefficient': lambda *args: 0,
        'last_child': lambda *args: True,
        'level': lambda *args: 0,
        }

    _sql_constraints = [
        ('name_uk', 'unique(name, course_id)', _('Parameter must be unique per course')),
        ('coefficient_ck', 'check(coefficient > 0 and coefficient <= 1)',
         _('Coefficient must be greater than 0 and equal or less than 1')),
        ]

    @api.one
    @api.depends('parent_id', 'course_ref', 'course_id')
    def _compute_course_ref(self):
        if self.parent_id:
            self.course_ref = self.parent_id.course_ref
        else:
            self.course_ref = self.course_id.id

    @api.one
    @api.depends('parent_ref', 'parent_id', 'course_id')
    def _compute_parent_ref(self):
        if self.parent_id:
            self.parent_ref = '%s-%s' % (self.parent_id.parent_ref, self.parent_id.name)
        else:
            self.parent_ref = self.course_id.name

    @api.one
    @api.depends('param_name')
    def _compute_name(self):
        if self.param_name:
            self.name = self.param_name.name

    @api.one
    @api.constrains('child_ids', 'name')
    def _check_coefficient(self):
        if self.child_ids:
            total = sum(record.coefficient for record in self.child_ids)
            if total != 1:
                    raise ValidationError("Sum of coefficients must be equal to 1, parameter: " + self.name)
            else:
                self.last_child = False
        else:
            self.last_child = True

    @api.one
    @api.depends('parent_id')
    def _compute_level(self):
        if self.parent_id:
            level = self.parent_id.level + 1
            self.level = level
        else:
            self.level = 0

    @api.one
    @api.depends('total_coefficient', 'child_ids')
    def _total_coefficient(self):
        if self.child_ids:
            total_coefficient = sum(o.coefficient for o in self.child_ids)
            self.total_coefficient = total_coefficient
        else:
            self.total_coefficient = 0
            self.last_child = True

    @api.onchange('total_coefficient')
    def _onchange_total_coefficient(self):
        if self.total_coefficient > 0:
            self.last_child = False
            if self.total_coefficient > 1:
                #desactivar botones de grabar
                raise Warning("Sum of coefficients must be equal to 1")
            #else:
                #activar botones de grabar
        else:
            self.last_child = True
