from openerp import fields, models, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import ValidationError, Warning
from dateutil.relativedelta import relativedelta
from ..misc import YEAR, COURSE_STATE, PERIOD, DURATION
import logging
_logger = logging.getLogger(__name__)


class sie_course(models.Model):
    _name = 'sie.course'
    _description = 'Course'

    name = fields.Char(string='Name', compute='_compute_display_name', store=True)
    lastname = fields.Char(string='Lastname', required=True)
    year = fields.Selection(YEAR, string='Year', required=True,  default='1')
    period = fields.Selection(PERIOD, string='Period', required=True,  default='1')
    start_date = fields.Date(string='Start Date', required=True)
    end_date = fields.Date(string='End Date', required=True)
    state = fields.Selection(COURSE_STATE, 'State', default='planned')
    minimum_score = fields.Integer(string='Overall Average to Pass', required=True, readonly=True,
                                   states={'planned': [('readonly', False)]})
    summary_ids = fields.One2many(comodel_name='sie.course.summary', inverse_name='course_id', string='Summary',
                                  store=True, readonly=True,
                                  states={'planned': [('readonly', False)]})
    parameter_ids = fields.One2many(comodel_name='sie.course.parameter', inverse_name='course_id',
                                    string='Parameters', store=True, readonly=True,
                                    states={'planned': [('readonly', False)], 'running': [('readonly', False)]})
    subject_ids = fields.Many2many(comodel_name='sie.subject', string="Subjects", required=True, store=True)
    no_of_subject = fields.Integer(compute='_compute_total', string='No Subjects', store=True)
    total_hours = fields.Integer(compute='_compute_total', string='No Hours', store=True)
    total_credits = fields.Integer(compute='_compute_total', string='No Credits', store=True)
    is_conduct = fields.Boolean(string='Is conduct', default=False)
    min_conduct = fields.Integer(string='MinConduct', help='Minimum of Conduct', required=True)
    max_demerits = fields.Integer(string='MaxDemerits', help='Maximum of Demerits', required=True)
    coefficient_conduct = fields.Float(compute='_coefficient_conduct', string='Coefficient',
                                       digits_compute=dp.get_precision('Calculation'), store=True)
    evaluator_id = fields.Many2one(comodel_name='sie.faculty', string='Evaluator', ondelete='restrict', readonly=True)

    _order = 'year, period'

    _defaults = {
        'state': lambda *args: 'planned',
        'minimum': lambda *args: 15,
        'min_conduct': lambda *args: 14,
        'max_demerits': lambda *args: 80,
        'minimum_score': lambda *args: 15,
        }

    _sql_constraints = [
        ('name_uk', 'unique(name)', _('Should not repeat promotion for course')),
        ('min_ck', 'check(minimum_score > 0 AND minimum_score < 20)',
         _('Overall average to pass must be between 1 and 19')),
        ('min_conduct_ck', 'check(min_conduct BETWEEN 0 AND 20)', 'Minimum of conduct must be between 0 and 20'),
        ('max_demerits_ck', 'check(max_demerits > 0)', 'Maximum of demerits must be greater than 0'),
        ]

    @api.one
    @api.constrains('end_date', 'start_date')
    def _check_end_date(self):
        if self.start_date and self.end_date:
            start_date = fields.Datetime.from_string(self.start_date)
            end_date = fields.Datetime.from_string(self.end_date)
            if end_date < start_date:
                end_date = start_date + relativedelta(months=+ 3)
                self.end_date = end_date
                raise Warning("La fecha de fin de curso tiene que ser mayor a la fecha de inicio")
        else:
            raise ValidationError("Seleccione las fecha de inicio y fin de curso")

    @api.one
    @api.constrains('parameter_ids')
    def _check_coefficient(self):
        if self.parameter_ids:
            total = sum(record.coefficient for record in self.parameter_ids)
            if total != 1:
                    raise ValidationError("Sum of coefficients must be equal to 1")

    @api.one
    @api.depends('subject_ids', 'no_of_subject', 'total_hours', 'total_credits')
    def _compute_total(self):
        self.no_of_subject = len(self.subject_ids)
        if self.no_of_subject > 0:
            self.total_hours = sum(record.hours for record in self.subject_ids)
            self.total_credits = sum(record.credits for record in self.subject_ids)
        else:
            raise Warning("Seleccine al menos una asignatura!")


    @api.one
    @api.depends('min_conduct', 'max_demerits')
    def _coefficient_conduct(self):
        min_conduct = self.min_conduct
        max_demerits = self.max_demerits
        self.coefficient_conduct = (float(20) - float(min_conduct)) / float(max_demerits)

    @api.one
    @api.depends('start_date', 'year', 'period', 'end_date', 'lastname')
    def _compute_display_name(self):
        if self.start_date and self.end_date and self.lastname:
            start_date = fields.Datetime.from_string(self.start_date)
            name = '%s-%s-%s-%s' % (self.lastname, start_date.year, self.year, self.period)
            self.name = name


    @api.one
    def action_plan(self):
        self.state = 'planned'

    @api.one
    def action_run(self):
        self.state = 'running'

    @api.one
    def action_done(self):
        self.state = 'finalized'

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        start_date = fields.Datetime.from_string(self.start_date)
        end_date = start_date + relativedelta(months=+ 12)
        default['start_date'] = end_date
        return super(sie_course, self).copy(default)
