from openerp import fields, models, api
from openerp.tools.translate import _


class sie_course_parameter_name(models.Model):
    _name = 'sie.course.parameter.name'
    _description = 'Course Parameter Name'

    name = fields.Char(string='Name', required=True)
    level = fields.Integer(string='Level', required=True, store=True)

    _defaults = {
        'level': lambda *args: 0,
        }

    _sql_constraints = [
        ('name_level_uk', 'unique(name,level)', _('Parameter must be unique per course')),
    ]