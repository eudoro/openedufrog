from . import misc
from . import sie_academic_title
from . import sie_award
from . import sie_category
from . import sie_concept
from . import sie_conduct
from . import sie_nato
from . import sie_medal_kind
from . import sie_grade
from . import sie_fault_article
from . import sie_fault_classification
from . import sie_fault_kind
from . import sie_fault_literal
from . import sie_official_rol
from . import sie_parameters
from . import sie_person_title
from . import sie_promotion
from . import sie_religion
from . import sie_physical_proof_table
from . import sie_physical_proof_test
from . import sie_physical_proof_param
from . import sie_fault_sanction
from . import sie_specialty
from . import sie_exempt_kind
from . import sie_merit
from . import sie_training_shaft
from . import sie_faculty
from . import sie_student
from . import sie_subject
from . import sie_course
from . import sie_enrollment
from . import sie_score
from . import sie_integrator_product
from . import sie_personal_appreciation
from . import sie_behavior
from . import sie_productivity
from . import sie_medical_leave
from . import sie_merit_control
from . import sie_sanction_control
from . import sie_military_training
from . import sie_physical_proof

#from . import sie_physical_proof_score

import report
#import wizard
