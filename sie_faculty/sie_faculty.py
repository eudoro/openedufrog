from openerp import models, fields, api, tools
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from ..misc import _check_verification_identification_id, BLOOD_TYPE, GENDER, FACULTY_TYPE
import logging
_logger = logging.getLogger(__name__)


class sie_faculty(models.Model):
    _name = 'sie.faculty'

    title = fields.Many2one('sie.person.title', string='Title')
    name = fields.Char(string='Name', required=True)
    email = fields.Char('Email', required=True)
    phone = fields.Char('Phone')
    mobile = fields.Char('Mobile')
    image = fields.Binary("Image",
                          help="This field holds the image used as avatar for this contact, limited to 1024x1024px")
    has_image = fields.Boolean(compute='_has_image')


    birth_date = fields.Datetime(string='Birth Date')
    blood_group = fields.Selection(BLOOD_TYPE, string='Blood Group')
    gender = fields.Selection(GENDER, 'Gender', required=True, default='male')
    nationality = fields.Many2one(comodel_name='res.country', string='Nationality')
    language = fields.Many2one(comodel_name='res.lang', string='Language')
    category = fields.Many2one(comodel_name='sie.category', string='Category')
    religion = fields.Many2one(comodel_name='sie.religion', string='Religion')
    identification_id = fields.Char(size=64, string='Identification Id')
    bank_acc_num = fields.Char(size=64, string='Bank Acc Number')
    id_number = fields.Char(size=64, string='ID Card Number')

    street = fields.Char('Street')
    street2 = fields.Char('Street2')
    zip = fields.Char('Zip', size=24, )
    city = fields.Char('City')
    parrish = fields.Char('Parrish')
    country_id = fields.Many2one(comodel_name='res.country', string='Country', ondelete='restrict')

    display_title_name = fields.Char(string='Display Name', compute='_compute_display_title_name')
    type = fields.Selection(FACULTY_TYPE, string='Type', default='civil', required=True)
    grade_id = fields.Many2one(comodel_name='sie.grade', string=_('Grade'), ondelete='cascade')
    specialty_id = fields.Many2one(comodel_name='sie.specialty', string="Specialty")
    official_rol = fields.Many2one(comodel_name='sie.official.rol', string=_('Rol'))
    is_coordinator = fields.Boolean(String="Is Coordinator")
    is_statistician = fields.Boolean(String="Is Statistician")
    user_id = fields.Many2one(comodel_name='res.users', string='User of OpenERP',
                              help='The internal user that is in charge of communicating with this contact if any.')

    comment = fields.Text('Notes')


    @api.onchange('identification_id')
    def _check_identification(self):
        if self.identification_id:
            unicodestring = self.identification_id
            nuc = str(unicodestring).encode("utf-8")
            if not _check_verification_identification_id(nuc):
                raise ValidationError(u'Cedula incorrecta')

    @api.one
    @api.depends('name', 'grade_id', 'specialty_id', 'title')
    def _compute_display_title_name(self):
        if self.grade_id and self.specialty_id:
            prefix = '%s-%s' % (self.grade_id.acronym, self.specialty_id.acronym)
        elif self.grade_id:
            prefix = '%s' % self.grade_id.acronym
        elif self.specialty_id:
            if self.title:
                prefix = '%s' % self.title.acronym
            else:
                prefix = ''
        else:
            if self.title:
                prefix = '%s' % self.title.acronym
            else:
                prefix = ''
        if len(prefix) > 0:
            display_name = '%s %s' % (prefix, self.name)
        else:
            display_name = self.name
        self.display_title_name = display_name

    @api.multi
    def _has_image(self, name, args):
        return dict((p.id, bool(p.image)) for p in self)

    @api.multi
    def _get_image(self, name, args):
        return dict((p.id, tools.image_get_resized_images(p.image)) for p in self)

    @api.one
    def _set_image(self, name, value, args):
        return self.write({'image': tools.image_resize_image_big(value)})

    @api.model
    def _lang_get(self):
        languages = self.env['res.lang'].search([])
        return [(language.code, language.name) for language in languages]