from openerp import fields, models, api
from openerp.tools.translate import _
from ..misc import SCORE_STATE
import time
import logging
_logger = logging.getLogger(__name__)


class sie_score(models.Model):
    _name = 'sie.score'
    _description = 'Score'
    _inherit = 'ir.needaction_mixin'

    name = fields.Char(string='Name', compute='_compute_display_name', store=True)
    date = fields.Date(string='Date', required=True)
    notes = fields.Text(string='Notes')
    course_id = fields.Many2one(comodel_name='sie.course', string='Course', domain="[('state', '=', 'running')]",
                                ondelete='restrict', required=True)
    enrollment_id = fields.Many2one(comodel_name='sie.enrollment', string='Division', ondelete='restrict',
                                    domain="[('course_id', '=', course_id)]", required=True)
    subject_id = fields.Many2one(comodel_name='sie.subject', string='Subject', ondelete='restrict', required=True)
    parameter_id = fields.Many2one(comodel_name='sie.course.parameter', string='Parameter', ondelete='restrict',
                                   domain="[('last_child', '=', True), "
                                          "'|',('parent_ref', 'like', 'APROVECHAMIENTO'),"
                                          "('name', 'like', 'APROVECHAMIENTO'),"
                                          "('course_ref', '=', course_id)]", required=True)

    faculty_id = fields.Many2one(comodel_name='sie.faculty', string='Evaluator', ondelete='restrict',
                                 domain="[('id', '=', uid)]", required=False)
    student_ids = fields.One2many(comodel_name='sie.score.student', inverse_name='score_id', string='Students')
    state = fields.Selection(SCORE_STATE, string='State')
    is_readonly = fields.Boolean(string='Is readonly?')

    _defaults = {
        'date': lambda *args: time.strftime('%Y-%m-%d'),
        'state': lambda *args: 'draft',
    }

    def _needaction_domain_get(self, cr, uid, context=None):
        return [('state', '=', 'for review'), ('faculty_id', '=', uid)]

    @api.onchange('enrollment_id')
    def _onchange_enrollment_id(self):
        students = []
        for student in self.enrollment_id.student_ids:
            data = {
                'name': student.identification_id,
                'student_id': student.id,
                'score': -1,
            }
            students.append(data)
        self.student_ids = students

    @api.multi
    @api.depends('course_id', 'subject_id', 'parameter_id', 'date', 'name', 'faculty_id')
    def _compute_display_name(self):
        if self.course_id and self.subject_id and self.parameter_id:
            create_date = time.strftime('%Y-%m-%d')
            name = '%s | %s | %s | %s' % (self.enrollment_id.name, self.subject_id.name,
                                          self.parameter_id.name, create_date)
            self.faculty_id = self.subject_id.responsible_id.id,
            self.name = name

    @api.one
    def action_publish(self):
        self.state = 'published'
        self.is_readonly = True

    @api.one
    def action_reject(self):
        self.state = 'for review'

    @api.one
    def action_approve(self):
        self.state = 'approved'

    def unlink(self, cr, uid, ids, context=None):
        unlink_ids = []
        for record in self.browse(cr, uid, ids):
            if record.state in ('approved'):
                raise models.Model.except_osv(_('Invalid Action!'), _('You can not delete an record which was settled'))
            else:
                unlink_ids.append(record.id)
        super(sie_score, self).unlink(cr, uid, unlink_ids, context=context)
        return True