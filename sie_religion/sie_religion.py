from openerp import models, fields
from openerp.tools.translate import _


class sie_religion(models.Model):
    _name = 'sie.religion'

    name = fields.Char(string='Name', required=True, select=True)

    _sql_constraints = [
        ('name_uk', 'unique(name)', _('Name must be unique'))
    ]
