from openerp import fields, models, api
from openerp.exceptions import ValidationError, Warning


class sie_enrollment(models.Model):
    _name = 'sie.enrollment'
    _description = 'Enrollment'

    name = fields.Char(string='Division', compute='_compute_fullname', store=True)
    nato = fields.Many2one('sie.nato', string='NATO phonetic alphabet', size=20, required=True)
    course_id = fields.Many2one('sie.course', string='Course', ondelete='restrict', required=True, domain=[('state', '=', 'planned')])
    student_ids = fields.Many2many(comodel_name='sie.student', store=True)
    no_of_students = fields.Integer(string='No of students', store=True)

    _order = 'name, course_id'

    _sql_constraints = [
        ('name_uk', 'unique(name, course_id)', 'Classroom must be unique per course'),
    ]

    @api.multi
    @api.constrains('student_ids')
    def _check_student(self):
        if len(self.student_ids) == 0:
            raise ValidationError("Must enroll at least one student")
        else:
            for record in self.student_ids:
                query = """ SELECT
                        sie_course.name as course,
                        sie_student.name as student,
                        count(*) as total
                    FROM
                        public.sie_enrollment_sie_student_rel,
                        public.sie_enrollment,
                        public.sie_course,
                        public.sie_student
                    WHERE
                        sie_enrollment_sie_student_rel.sie_enrollment_id = sie_enrollment.id AND
                        sie_enrollment_sie_student_rel.sie_student_id = sie_student.id AND
                        sie_enrollment.course_id = sie_course.id AND
                        sie_enrollment_sie_student_rel.sie_student_id = %s AND
                        sie_course.state = 'planned'
                    GROUP BY course, student
                """
                self._cr.execute(query, (record.id,))
                for course, student, total in self._cr.fetchall():
                    if total > 1:
                        raise Warning(student + ' ya se encuentra enrrolado en el curso ' + course)
            self.no_of_students = len(self.student_ids)

    @api.one
    @api.depends('name', 'course_id', 'nato')
    def _compute_fullname(self):
        if self.nato and self.course_id:
            fullname = '%s-%s' % (self.nato.display_name, self.course_id.name)
            self.name = fullname