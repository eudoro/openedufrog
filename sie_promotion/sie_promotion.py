from openerp import models, fields, api, _
from openerp.exceptions import ValidationError


class sie_promotion(models.Model):
    _name = 'sie.promotion'

    name = fields.Char(string='Name', required=True)
    display_name = fields.Char(string='Display Name', compute='_compute_display_name')

    @api.one
    @api.depends('name')
    def _compute_display_name(self):
        if self.name:
            prefix = "Promocion"
            self.display_name = '%s %s' % (prefix, self.name)

    @api.onchange('name')
    def _check_digit(self):
        if self.name:
            unicodestring = self.name
            s = str(unicodestring).encode("utf-8")
            try:
                float(s)
            except ValueError:
                raise ValidationError(u'No es un numero')

    @api.multi
    @api.depends('display_name')
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, '%s' % record.display_name))
        return result