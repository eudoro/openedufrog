from openerp import fields, models, api
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from ..misc import CONTROL_STATE
import time
import logging
_logger = logging.getLogger(__name__)


class sie_physical_proof(models.Model):
    _name = 'sie.physical.proof'
    _description = 'Physical Proof'

    name = fields.Char(string='Name', compute='_compute_display_name', store=True)
    date = fields.Date(string='Date', required=True)
    notes = fields.Text(string='Notes')
    course_id = fields.Many2one(comodel_name='sie.course', string='Course', domain="[('state', '=', 'running')]",
                                ondelete='restrict', required=True)
    enrollment_id = fields.Many2one(comodel_name='sie.enrollment', string='Division', ondelete='restrict',
                                    domain="[('course_id', '=', course_id)]", required=True)
    student_ids = fields.One2many(comodel_name='sie.physical.proof.student', inverse_name='physical_proof_id',
                                  string='Students')
    state = fields.Selection(CONTROL_STATE, 'State')
    is_readonly = fields.Boolean(string='Is readonly?')


    _defaults = {
        'state': lambda *args: 'draft',
    }

    @api.multi
    @api.depends('enrollment_id', 'date')
    def _compute_display_name(self):
        if self.course_id and self.date:
            create_date = time.strftime('%Y%m%d%H%M%S')
            name = '%s | %s ' % (self.enrollment_id.name,create_date)
            self.name = name

    @api.onchange('enrollment_id')
    def _onchange_enrollment_id(self):
        students = []
        for student in self.enrollment_id.student_ids:
            table_obj = self.env['sie.physical.proof.table']
            domain = [('from_included', '<=', student.age)]
            domain += [('to_not_included', '>', student.age)]
            record = table_obj.search(domain)
            data = {
                'name': student.identification_id,
                'student_id': student.id,
                'gender': student.gender,
                'table': record.id,
                'run': '-1',
                'abdominal': '-1',
                'push_ups': '-1',
                'natation': '-1',
                'race': '-1',
                'climb': '-1',
                'flotation': '-1',
            }
            students.append(data)
        self.student_ids = students

    #def _compute_score(self):
        #cr.execute("""WITH x (proof_id, score, score_ref) AS (
		#		SELECT proof_id, sum(score), sum(score_ref)
		#		FROM sie_physical_proof_score
		#		WHERE proof_id IN %s
		#		GROUP BY proof_id
		#	) SELECT proof_id, 20 * score / score_ref FROM X""", (tuple(ids), ))
       #res = dict(cr.fetchall())
       # for id in ids:
            # obj = self.browse(cr, uid, id)
            #_logger.info(res.get(id, 0))
        #    res[id] = res.get(id, 0)
       # return res



    def publish(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'published'}, context=context)
        return True

    def settle(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'settled'}, context=context)
        return True

    def create(self, cr, uid, vals, context=None, check=True):
        e_obj = self.pool.get('sie.enrollment').browse(cr, uid, vals.get('enrollment_id'))
        vals['course_id'] = e_obj.course_id.id
        return super(sie_physical_proof, self).create(cr, uid, vals, context)

    def copy(self, cr, uid, id, default=None, context=None):
        if not context:
            context = {}
        if not default:
            default = {}
        record = self.browse(cr, uid, id, context=context)
        default = default.copy()
        default['state'] = 'draft'
        return super(sie_physical_proof, self).copy(cr, uid, id, default, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        record = self.browse(cr, uid, ids[0])
        e_obj = record.enrollment_id
        # print vals
        if e_obj.course_id:
            vals['course_id'] = e_obj.course_id.id
        return super(sie_physical_proof, self).write(cr, uid, ids, vals, context)

    def unlink(self, cr, uid, ids, context=None):
        unlink_ids = []
        for record in self.browse(cr, uid, ids):
            if record.state in ('settled'):
                raise models.Model.except_osv(_('Invalid Action!'), _('You can not delete an record which was settled'))
            else:
                unlink_ids.append(record.id)
        super(sie_physical_proof, self).unlink(cr, uid, unlink_ids, context=context)
        return True