from openerp import models, fields, api
import openerp.addons.decimal_precision as dp
from .. misc import number_value_pattern, time_value_pattern


class sie_physical_proof_student(models.Model):
    _name = 'sie.physical.proof.student'
    _description = 'Student\'s Physical Proof Score'

    name = fields.Char(string='ID', store=True)
    student_id = fields.Many2one(comodel_name='sie.student', string='Student', ondelete='restrict',
                                 required=True, store=True)
    grade = fields.Char(string='Grade', store=True)
    gender = fields.Char(string='Gender', store=True)
    table = fields.Integer(string='Table', store=True)
    run = fields.Char('Score Run', required=True)
    abdominal = fields.Char('Score Abdominal', required=True)
    push_ups = fields.Char('Score Push-Ups', required=True)
    natation = fields.Char('Score Natation', required=True)
    race = fields.Char('Score Race', required=True)
    climb = fields.Char('Score Climb',  required=True)
    flotation = fields.Char('Score Flotation',  required=True)
    score = fields.Float('Score', digits_compute=dp.get_precision('Score'), required=True)
    physical_proof_id = fields.Many2one(comodel_name='sie.physical.proof', string='Physical Proof ID',
                                        ondelete='cascade')

    _order = 'name, student_id'

    _defaults = {
        'score': lambda *args: -1,
        }

    _sql_constraints = [
        ('score_ck', 'check(score between 0 and 20)', 'Score must be between 0 and 20'),
        ]

    @api.onchange('run')
    def _check_run(self):
        match = time_value_pattern.match(self.run)
        if not match:
            self.run = 'error!'

    @api.onchange('abdominal')
    def _check_abdominal(self):
        match = number_value_pattern.match(self.abdominal)
        if not match:
            self.abdominal = 'error!'

    @api.onchange('push_ups')
    def _check_push_ups(self):
        match = number_value_pattern.match(self.push_ups)
        if not match:
            self.push_ups = 'error!'

    @api.onchange('natation')
    def _check_natation(self):
        match = time_value_pattern.match(self.natation)
        if not match:
            self.natation = 'error!'

    @api.onchange('race')
    def _check_race(self):
        match = time_value_pattern.match(self.race)
        if not match:
            self.race = 'error!'

    @api.onchange('climb')
    def _check_climb(self):
        match = time_value_pattern.match(self.climb)
        if not match:
            self.climb = 'error!'

    @api.onchange('flotation')
    def _check_flotation(self):
        match = time_value_pattern.match(self.flotation)
        if not match:
            self.flotation = 'error!'

    @api.one
    @api.depends('run', 'abdominal', 'push_ups', 'natation', 'race', 'climb', 'flotation')
    def _compute_score_total(self):
        if self.run != 'error!' and self.abdominal != 'error!' and self.push_ups != 'error!' \
                and self.natation != 'error!' and self.race != 'error!' and self.climb != 'error!' \
                and self.flotation != 'error!':
            total = 0
            if int(self.run) > -1:
                total_run = sie_physical_proof_student._compute_score(self, self.run)
            else:
                total_run = 0
            if int(self.abdominal) > -1:
                total_abdominal = sie_physical_proof_student._compute_score(self, self.abdominal)
            else:
                total_abdominal = 0
            if int(self.push_ups) > -1:
                total_push_ups = sie_physical_proof_student._compute_score(self, self.push_ups)
            else:
                total_push_ups = 0
            if int(self.natation) > -1:
                total_natation = sie_physical_proof_student._compute_score(self, self.natation)
            else:
                total_natation = 0
            if int(self.race) > -1:
                total_race = sie_physical_proof_student._compute_score(self, self.race)
            else:
                total_race = 0
            if int(self.climb) > -1:
                total_climb = sie_physical_proof_student._compute_score(self, self.climb)
            else:
                total_climb = 0
            if int(self.flotation) > -1:
                total_flotation = sie_physical_proof_student._compute_score(self, self.flotation)
            else:
                total_flotation = 0
            total = total_run + total_abdominal + total_push_ups + total_natation + total_race + total_climb + total_flotation
            param_obj = self.env['sie.physical.proof.param']
            domain = [('table_id', '=', self.table)]
            domain += [('gender', '=', self.gender)]
            param_ids = param_obj.search(domain)
            score = 0
            total_ref = 0
            for rec in param_ids:
                test_obj = self.env['sie.physical.proof.test']
                domain = [('id', '=', rec.test_id)]
                test_id = param_obj.search(domain)
                total_ref = total_ref + test_id.score
            score = 20 * total / total_ref
            self.score = score

    def _compute_score(self, in_value):
        value = in_value
        param_obj = self.env['sie.physical.proof.param']
        domain = [('table_id', '=', self.table)]
        domain += [('gender', '=', self.gender)]
        param_ids = param_obj.search(domain)
        score = 0

        for rec in param_ids:
            total = 0
            test_obj = self.env['sie.physical.proof.test']
            domain = [('id', '=', rec.test_id)]
            test_id = test_obj.search(domain)
            if rec.measure == 'time':
                m, s = in_value.split(',')
                seconds = (int(m) * 60) + int(s)
                if rec.control == 'exceed':
                    # if exceed then 0
                    if seconds >= int(rec.value):
                        total = test_id.score
                    else:
                        # (rec.score_ref - (rec.int_value - seconds)) * rec.coefficient
                        total = 0
                elif rec.control == 'between':
                    if seconds <= int(rec.value):
                        total = test_id.score
                    elif seconds > int(rec.max_value):
                        total = 0
                    else:
                        total = test_id.score - ((seconds - int(rec.value)) / rec.coefficient)
                else:  # if not exceed then 0
                    if seconds <= int(rec.value):
                        total = test_id.score
                    else:
                        # (rec.score_ref - (seconds - rec.int_value)) * rec.coefficient
                        total = 0
            else:
                value = int(in_value)
                if value == 0:
                    total = 0
                if value >= int(rec.value):
                    total = test_id.score
                else:
                    coefficient = test_id.score / int(rec.value)
                    total = value * coefficient
            score = score + total
        return score
