from openerp import models, fields, api


class sie_conduct(models.Model):
    _name = 'sie.conduct'

    name = fields.Char(string='Name', compute='_compute_name', store=True)
    code = fields.Integer(string='Year', required=True)
    min_conduct = fields.Integer(string='MinConduct', help='Minimum of Conduct', required=True)
    max_demerits = fields.Integer(string='MaxDemerits', help='Maximum of Demerits', required=True)

    _order = 'name'

    _sql_constraints = [
        ('name_uk', 'unique(name)', 'Concept must be unique'),
    ]

    @api.one
    @api.depends('name', 'code')
    def _compute_name(self):
        if self.code:
            name = 'Conduct Year - %s' % (self.code,)
            self.name = name

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.search_count(
            [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)
        default['name'] = new_name
        return super(sie_conduct, self).copy(default)