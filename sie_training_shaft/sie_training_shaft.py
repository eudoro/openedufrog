from openerp import fields, models


class sie_training_shaft(models.Model):
    _name = 'sie.training.shaft'
    _description = 'Shaft of Training'

    name = fields.Char('Name', size=96, required=True)
    subject_ids = fields.One2many('sie.subject', 'shaft_id', 'Subjects')

    _order = 'name'

    _sql_constraints = [
        ('name_uk', 'unique(name)', 'Shaft of training must be unique'),
    ]

    def create(self, cr, uid, vals, context=None, check=True):
        vals['name'] = vals['name'].title()
        return super(sie_training_shaft, self).create(cr, uid, vals, context)

    def write(self, cr, uid, id, vals, context=None):
        name = False
        if vals.get('name'):
            name = vals['name']
        else:
            obj = self.browse(cr, uid, id)
            name = obj[0].name
        vals.update({'name': name.title()})
        return super(sie_training_shaft, self).write(cr, uid, id, vals, context=context)