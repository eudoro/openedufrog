from openerp import models, fields
from openerp.tools.translate import _


class sie_official_rol(models.Model):
    _name = 'sie.official.rol'

    name = fields.Char(string='Name', required=True, select=True)

    _sql_constraints = [
        ('name_uk', 'unique(name)', _('Name must be unique')),
    ]