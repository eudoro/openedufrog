from openerp import models, fields
from openerp.tools.sql import drop_view_if_exists


class sie_subject_faculty_report(models.Model):
    _name = 'sie.subject.faculty.report'
    _description = 'I teach'
    _auto = False

    name = fields.Char('Subject', size=96, required=True)
    code = fields.Char('Code', size=10,)
    version = fields.Float('Version', digits=(2, 1), readonly=True)
    senescyt_code = fields.Char('SENESCYT\'s code', size=64, readonly=True)
    shaft_id = fields.Many2one(comodel_name='sie.training.shaft', string='Shaft of training', readonly=True)
    credits = fields.Integer('Credits', readonly=True)
    hours = fields.Integer('Hours', readonly=True)
    faculty_id = fields.Many2one(comodel_name='sie.faculty', string='faculty', readonly=True)

    _order = 'name, version'

    def init(self, cr):
        drop_view_if_exists(cr, 'sie_subject_faculty_report')
        cr.execute("""CREATE VIEW sie_subject_faculty_report as (
            SELECT
                sie_subject.id as id,
                sie_subject.name,
                sie_subject.code,
                sie_subject.version,
                sie_subject.senescyt_code,
                sie_subject.shaft_id,
                sie_subject.credits,
                sie_subject.hours,
                sie_faculty.id as faculty_id
            FROM
                public.sie_subject,
                public.sie_faculty,
                public.sie_course,
                public.sie_enrollment,
                public.sie_course_sie_subject_rel
            WHERE
                sie_subject.responsible_id = sie_faculty.id AND
                sie_enrollment.course_id = sie_course.id AND
                sie_course_sie_subject_rel.sie_course_id = sie_course.id AND
                sie_course_sie_subject_rel.sie_subject_id = sie_subject.id
            )
        """)


    def edit(self, cr, uid, ids, context=None):
        return {
            'name': 'Subject',  # 'context': context,
            'res_model': 'sie.subject',
            'res_id': ids[0],
            'target': 'current',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'view_mode': 'form',
            'view_type': 'form',
        }