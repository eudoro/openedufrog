from openerp import models, fields, tools, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _
from dateutil.relativedelta import relativedelta
from ..misc import BLOOD_TYPE, GENDER
from ..misc import _check_verification_identification_id
import logging
_logger = logging.getLogger(__name__)

class sie_student(models.Model):
    _name = 'sie.student'

    title = fields.Many2one('sie.person.title', string='Title')
    name = fields.Char(string='Name', required=True, select=True)
    email = fields.Char('Email')
    phone = fields.Char('Phone')
    mobile = fields.Char('Mobile')

    birth_date = fields.Date(string='Birth Date', required=True)
    blood_group = fields.Selection(BLOOD_TYPE, string='Blood Group')
    gender = fields.Selection(GENDER, string='Gender', required=True, default='male')
    nationality = fields.Many2one('res.country', string='Nationality')
    language = fields.Many2one(comodel_name='res.lang', string='Language')
    religion = fields.Many2one(comodel_name='sie.religion', string='Religion')
    library_card = fields.Char(size=64, string='Library Card')
    identification_id = fields.Char(string='Identification Id', required=True)
    bank_acc_num = fields.Char(size=64, string='Bank Acc Number')
    id_number = fields.Char(size=64, string='ID Card Number')

    display_title_name = fields.Char(compute='_compute_display_title_name', string='Display Name', store=True)
    admission_date = fields.Date(string='Admission Date', required=True)
    academic_title_ids = fields.Many2many(comodel_name='sie.academic.title', string='Academic Titles')
    promotion_id = fields.Many2one(comodel_name='sie.promotion', string="Promotion", required=True)
    grade_id = fields.Many2one(comodel_name='sie.grade', string=_('Grade'), ondelete='cascade', required=True)
    specialty_id = fields.Many2one(comodel_name='sie.specialty', string="Specialty", required=True)
    award_ids = fields.Many2many(comodel_name='sie.award', string='Awards')
    age = fields.Integer(compute='_compute_age', string=_('Age'))
    force_years = fields.Integer(compute='_compute_force_years', string=_('Force Years'))

    user_id = fields.Many2one(comodel_name='res.users', string='User',
                              help='The internal user that is in charge of communicating with this contact if any.')

    shirt = fields.Char(_('Shirt'), size=32)
    pant = fields.Char(_('Pant'), size=32)
    shoe = fields.Char(_('Shoe'), size=32)
    cap = fields.Char(_('Cap'), size=32)



    comment = fields.Text('Notes')

    street = fields.Char('Street')
    street2 = fields.Char('Street2')
    zip = fields.Char('Zip', size=24, change_default=True)
    city = fields.Char('City')
    country_id = fields.Many2one('res.country', 'Country', ondelete='restrict')


    photo1 = fields.Binary("Image",
                           help="This field holds the image used as avatar for this contact, limited to 1024x1024px")
    photo2 = fields.Binary("Image",
                           help="This field holds the image used as avatar for this contact, limited to 1024x1024px")

    image = fields.Binary("Image",
                          help="This field holds the image used as avatar for this contact, limited to 1024x1024px")
    has_image = fields.Boolean(compute='_has_image', type="boolean")
    is_active = fields.Boolean(string='is active', type="boolean", default=True)

    @api.one
    @api.depends('birth_date', 'age')
    def _compute_age(self):
        if self.birth_date:
            birth_date = fields.Datetime.from_string(self.birth_date)
            _logger.warning("Anio nacimiento: %s" % birth_date)
            today = fields.Datetime.from_string(fields.Datetime.now())
            #_logger.warning("Hoy: %s" % today)
            if today >= birth_date:
                age = relativedelta(today, birth_date)
                #_logger.warning(age)
                self.age = age.years
                #_logger.warning("Edad: %s" % self.age)

    @api.one
    @api.depends('admission_date', 'force_years')
    def _compute_force_years(self):
        if self.admission_date:
            admission_date = fields.Datetime.from_string(self.admission_date)
            _logger.warning("Anio ingreso: %s" % admission_date)
            today = fields.Datetime.from_string(fields.Datetime.now())
            _logger.warning("Hoy: %s" % today)
            if today >= admission_date:
                calculate_age = relativedelta(today, admission_date)
                #_logger.warning(calculate_age)
                self.force_years = calculate_age.years
                #_logger.warning("Anio en la fuerza: %s " % self.force_years)

    @api.onchange('identification_id')
    def _check_identification(self):
        if self.identification_id:
            unicodestring = self.identification_id
            nuc = str(unicodestring).encode("utf-8")
            if not _check_verification_identification_id(nuc):
                raise ValidationError(u'Cedula incorrecta')

    @api.one
    @api.depends('name', 'grade_id', 'specialty_id', 'title')
    def _compute_display_title_name(self):
        if self.grade_id and self.specialty_id:
            prefix = '%s-%s' % (self.grade_id.acronym, self.specialty_id.acronym)
        elif self.grade_id:
            prefix = '%s' % self.grade_id.acronym
        elif self.specialty_id:
            if self.title:
                prefix = '%s' % self.title.acronym
            else:
                prefix = ''
        else:
            if self.title:
                prefix = '%s' % self.title.acronym
            else:
                prefix = ''
        if len(prefix) > 0:
            display_name = '%s %s' % (prefix, self.name)
        else:
            display_name = self.name
        self.display_title_name = display_name

    @api.multi
    @api.depends('display_title_name')
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, '%s' % record.display_title_name))
        return result

    @api.one
    def write(self, vals):
        name = False
        if vals.get('name'):
            name = vals['name']
        else:
            name = self.name
        vals.update({'name': name.title()})
        return super(sie_student, self).write(vals)

    @api.multi
    def _has_image(self, name, args):
        return dict((p.id, bool(p.image)) for p in self)

    @api.multi
    def _get_image(self, name, args):
        return dict((p.id, tools.image_get_resized_images(p.image)) for p in self)

    @api.one
    def _set_image(self, name, value, args):
        return self.write({'image': tools.image_resize_image_big(value)})

    @api.model
    def _lang_get(self):
        languages = self.env['res.lang'].search([])
        return [(language.code, language.name) for language in languages]