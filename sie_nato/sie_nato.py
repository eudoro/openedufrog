from openerp import models, fields, api, _
from openerp.exceptions import ValidationError


class sie_nato(models.Model):
    _name = 'sie.nato'

    name = fields.Char(string='Name', required=True)
    display_name = fields.Char(string='Display Name')

    @api.multi
    @api.depends('display_name')
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, '%s' % record.display_name))
        return result