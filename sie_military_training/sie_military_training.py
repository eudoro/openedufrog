import time
import logging
from openerp import fields, models, api
from openerp.osv import osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from ..misc import CONTROL_STATE
_logger = logging.getLogger(__name__)


class sie_military_training(models.Model):
    _name = 'sie.military.training'
    _description = 'Military Training'

    name = fields.Char(string='Name', compute='_compute_display_name', store=True)
    date = fields.Date(string='Date', required=True, readonly=True, states={'draft': [('readonly', False)]})
    notes = fields.Text('Notes')
    course_id = fields.Many2one(comodel_name='sie.course', string='Course', ondelete='restrict', required=True,
                                domain="[('state', '=', 'running')]")#,('evaluator_id', '==', uid)]")
    enrollment_id = fields.Many2one(comodel_name='sie.enrollment', string='Division', ondelete='restrict',
                                    domain="[('course_id', '=', course_id)]", required=True, readonly=True,
                                    states={'draft': [('readonly', False)]})
    evaluator_id = fields.Char(string='Evaluator', ondelete='restrict', readonly=True)
    parameter_id = fields.Many2one(comodel_name='sie.course.parameter', string='Parameter', ondelete='restrict',
                                   domain="[('last_child', '=', True),"
                                          "'|',('parent_ref', 'like', 'INSTRUCCION MILITAR'),"
                                          "('name', 'like', 'INSTRUCCION MILITAR'),('course_ref', '=', course_id)]",
                                   required=True)
    student_ids = fields.One2many(comodel_name='sie.integrator.product.student', inverse_name='score_id',
                                  string='Students', store=True)
    state = fields.Selection(CONTROL_STATE, string='State')
    is_readonly = fields.Boolean(string='Is readonly?')

    _order = 'name'

    _defaults = {
        'state': lambda *args: 'draft',
    }

    _sql_constraints = [
        ('military_training_uk', 'unique(course_id, enrollment_id, date)', 'Record must be unique'),
        ]

    @api.multi
    @api.depends('course_id', 'parameter_id', 'date', 'name', 'evaluator_id')
    def _compute_display_name(self):
        if self.course_id and self.subject_id and self.parameter_id:
            create_date = time.strftime('%Y%m%d-%H%M%S')
            name = '%s - INSTRUCCION MILITAR - %s - %s' % (self.enrollment_id.name,
                                                           self.parameter_id.name, create_date)
            self.evaluator_id = self.course_id.evaluator_id,
            self.name = name

    @api.onchange('enrollment_id')
    def onchange_enrollment(self):
        students = []
        for student in self.enrollment_id.student_ids:
            data = {
                'name': student.identification_id,
                'student_id': student.id,
                'score': -1,
            }
            students.append(data)
        self.student_ids = students

    def publish(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'published'}, context=context)
        return True

    def settle(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'settled'}, context=context)
        return True

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.search_count(
            [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)
        default['name'] = new_name
        default['state'] = 'draft'
        return super(sie_military_training, self).copy(default)

    @api.multi
    def unlink(self):
        for record in self:
            if record.state in ('settled'):
                raise Warning(_('You can not delete an record which was settled'))
        return models.Model.unlink(self)