from openerp import models, fields
import openerp.addons.decimal_precision as dp


class sie_military_training_score(models.Model):
    _name = 'sie.military.training.score'
    _description = 'Military Training Score'

    name = fields.Char(string='ID', store=True)
    student_id = fields.Many2one(comodel_name='sie.student', string='Student', ondelete='restrict',
                                 required=True, store=True)
    score = fields.Float('Score', digits_compute=dp.get_precision('Score'), required=True)
    score_id = fields.Many2one(comodel_name='sie.military.training', string='Score Military Training ID',
                               ondelete='cascade')

    _order = 'name, student_id'

    _defaults = {
        'score': lambda *args: -1,
        }

    _sql_constraints = [
        ('score_ck', 'check(score between 0 and 20)', 'Score must be between 0 and 20'),
        ]