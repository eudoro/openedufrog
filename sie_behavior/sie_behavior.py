from openerp import fields, models, api
from openerp.tools.translate import _
from ..misc import CONTROL_STATE
import time
import logging
_logger = logging.getLogger(__name__)


class sie_behavior(models.Model):
    _name = 'sie.behavior'
    _description = 'Behavior'
    _inherit = ['ir.needaction_mixin']

    name = fields.Char(string='Name', size=96, required=True, readonly=True)
    date = fields.Date(string='Date', required=True, readonly=True, states={'draft': [('readonly', False)]})
    notes = fields.Text(string='Notes')
    course_id = fields.Many2one(comodel_name='sie.course', string='Course', ondelete='restrict', required=True,
                                domain="[('state', '=', 'running')]",)#,('evaluator_id', '==', uid)]")
    enrollment_id = fields.Many2one(comodel_name='sie.enrollment', string='Division', ondelete='restrict',
                                    domain="[('course_id', '=', course_id)]", required=True, readonly=True,
                                    states={'draft': [('readonly', False)]})
    evaluator_id = fields.Char(string='Evaluator', ondelete='restrict', readonly=True)
    parameter_id = fields.Many2one(comodel_name='sie.course.parameter', string='Parameter', ondelete='restrict',
                                   domain="[('last_child', '=', True),"
                                          "'|',('parent_ref', 'like', 'COMPORTAMIENTO'),"
                                          "('name', 'like', 'COMPORTAMIENTO'),('course_ref', '=', course_id)]",
                                   required=True)
    student_ids = fields.One2many(comodel_name='sie.behavior.student', inverse_name='score_id',
                                  string='Students', store=True)
    state = fields.Selection(CONTROL_STATE, string='State')
    is_readonly = fields.Boolean(string='Is readonly?')

    _order = 'course_id, enrollment_id'

    _defaults = {
        'date': lambda *args: time.strftime('%Y-%m-%d'),
        'evaluator_id': lambda self, cr, uid, context=None: uid,
        'state': lambda *args: 'draft',
    }

    _sql_constraints = [
        ('behavior_uk', 'unique(course_id, enrollment_id, parameter_id)', 'Record must be unique'),
    ]

    @api.onchange('enrollment_id')
    def onchange_enrollment(self):
        students = []
        for student in self.enrollment_id.student_ids:
            data = {
                'name': student.identification_id,
                'student_id': student.id,
                'score': -1,
            }
            students.append(data)
        self.student_ids = students

    @api.multi
    @api.depends('course_id', 'parameter_id', 'date', 'name', 'evaluator_id')
    def _compute_display_name(self):
        if self.course_id and self.subject_id and self.parameter_id:
            create_date = time.strftime('%Y%m%d-%H%M%S')
            name = '%s - COMPORTAMIENTO - %s - %s' % (self.enrollment_id.name,
                                          self.parameter_id.name, create_date)
            self.evaluator_id = self.course_id.evaluator_id,
            self.name = name

    def publish(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'published'}, context=context)
        return True

    def settle(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'settled'}, context=context)
        return True

    def create(self, cr, uid, vals, context=None, check=True):
        e_obj = self.pool.get('sie.enrollment').browse(cr, uid, vals.get('enrollment_id'))
        vals['course_id'] = e_obj.course_id.id
        return super(sie_behavior, self).create(cr, uid, vals, context)

    def write(self, cr, uid, ids, vals, context=None):
        record = self.browse(cr, uid, ids[0])
        e_obj = record.enrollment_id
        # print vals
        if e_obj.course_id:
            vals['course_id'] = e_obj.course_id.id
        return super(sie_behavior, self).write(cr, uid, ids, vals, context)

    def unlink(self, cr, uid, ids, context=None):
        unlink_ids = []
        for record in self.browse(cr, uid, ids):
            if record.state in ('settled'):
                raise models.Model.except_osv(_('Invalid Action!'), _('You can not delete an record which was settled'))
            else:
                unlink_ids.append(record.id)
        super(sie_behavior, self).unlink(cr, uid, unlink_ids, context=context)
        return True