from openerp import models, fields, api
from openerp.tools.translate import _


class sie_specialty(models.Model):
    _name = 'sie.specialty'

    acronym = fields.Char(_('Acronym'), size=10, required=True, search='_search_acronym')
    name = fields.Char(_('Name'), size=96, required=True, search='_search_name')

    _sql_constraints = [
        ('acronym_uk', 'unique(acronym)', _('Acronym must be unique')),
        ('name_uk', 'unique(name)', _('Name must be unique')),
    ]

    def _search_name(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        if self.name:
                return [('name', operator, value)]

    def _search_acronym(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        if self.acronym:
            if len(self.acronym) == 4:
                return [('acronym', operator, value)]

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.search_count(
            [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
            new_acronym = u"Copy of {}".format(self.acronym)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)
            new_acronym = u"Copy of {} ({})".format(self.acronym, copied_count)
        default['name'] = new_name
        default['acronym'] = new_acronym
        return super(sie_specialty, self).copy(default)

